<div align="right">

###### ***AEROSPACE DESIGN PROJECT, EAS4947 SEMESTER 1, SESSION 2021/2022***

</div>

<div align="center">

<img src="https://gitlab.com/viknes10/idp-weekly-logbook/-/raw/main/Upm_Logo.png" width=350 align=middle>

</div>

<div align="center">

### :rocket: Department of Aerospace Engineering :rocket:
### EAS4947-1: PROJEK REKA BENTUK AEROANGKASA 
### (AEROSPACE DESIGN PROJECT)

</div>


## Front cover 


| <div align="center"> Heading | <div align="center"> Information  |
| :------: | :-----------: |
| Name | Vikneswaran A/L Devandran |
| Matric Number   | 197279 |
| Year of Study  | Year 4 |
| Subsystem Group | IDP Simulation |
| Team   | 1 |
| Lecturer / Supervisor   | DR. AHMAD SALAHUDDIN BIN MOHD HARITHUDDIN |

## Content of the Log

| <div align="center"> Log Book Contents | 
| :------: | 
| 1. Agenda   |
| 2. Goals   |
| 3. Problems   |
| 4. Decisions taken to solve problems   |
| 5. Method to solve problems   |
| 6. Justification   |
| 7. Impact of the decision taken into   |
| 8. Next progress or step   |


## Table of Logbook

| No. | Log Week | Log Link | 
| :------: | :------: | :------: |
| 1 | Week 1 | [link](https://gitlab.com/viknes10/idp-weekly-logbook#log-book-01-29th-october-2021)|
| 2 | Week 2 | [link](https://gitlab.com/viknes10/idp-weekly-logbook#log-book-02-5th-november-2021)|
| 3 | Week 3 | [link](https://gitlab.com/viknes10/idp-weekly-logbook#log-book-03-12th-november-2021)|
| 4 | Week 4 | [link](https://gitlab.com/viknes10/idp-weekly-logbook#log-book-04-19th-november-2021)|
| 5 | Week 5 | [link](https://gitlab.com/viknes10/idp-weekly-logbook#log-book-05-26th-november-2021)|
| 6 | Week 6 | [link](https://gitlab.com/viknes10/idp-weekly-logbook#log-book-06-3rd-december-2021)|
| 7 | Week 7 | [link](https://gitlab.com/viknes10/idp-weekly-logbook#log-book-07)|
| 8 | Week 8 | [link](https://gitlab.com/viknes10/idp-weekly-logbook#log-book-08)|
| 9 | Week 9 | [link](https://gitlab.com/viknes10/idp-weekly-logbook#log-book-09)|
| 10 | Week 10 | [link](https://gitlab.com/viknes10/idp-weekly-logbook#log-book-10)|
| 11 | Week 11 | [link](https://gitlab.com/viknes10/idp-weekly-logbook#log-book-11)|
| 12 | Week 12 | [link](https://gitlab.com/viknes10/idp-weekly-logbook#log-book-12)|
| 13 | Week 13 | [link](https://gitlab.com/viknes10/idp-weekly-logbook#log-book-13)|
| 14 | Week 14 | [link](https://gitlab.com/viknes10/idp-weekly-logbook#log-book-14)|
<!-- blank line -->
----
<!-- blank line -->
<div align="center">

## **Page 1**

</div>

<div align="center">

<img src="https://gitlab.com/viknes10/idp-logbook-1/-/raw/main/R__1_.png" width=200 align=middle>

</div>

## Log Book 01 [29th October 2021]

|<div align="center"> No. |<div align="center"> Content | <div align="center"> Descriptions |
| ------ | ------ | ------ |
|<div align="center"> 1 |<div align="center"> Agenda|I had a brief discussions among the group members under that subsytem called simulations. We had Discussed the project outline or worload progress for the simulation group for the whole milestone until week 8. Then, the simulation project outline was created and proposed through a modelled online based Gantt Chart. The simulation main task was proposed in the Gantt Chart are Real time measurements data collection, Catia Design Drawing, Flight Performamce Calculation and Computational Fluid Dynamics (ANSYS) analysis.|
| <div align="center">2 | <div align="center">Goals |To take the reading or data collection regards to the exact measurements of physical HAU in the LAB. Then, to have a simple ilustartions or the imagination on the HAU design especially of its airframe. |
|<div align="center"> 3 | <div align="center">Problem |We had a task dividing problems in term of group member. The certain group member are still outside the campus. |
| <div align="center">4 | <div align="center">Decision |Then, the group of certain students or group mates staying near campus was voluntered themselves in the group to do the first task. |
|<div align="center"> 5 |<div align="center"> Method |The group member had been planned out the timing and do the confirmation with the person in charge of the Lab facility to have an access in recording the data measurements. |
|<div align="center"> 6| <div align="center">Justifications |The progress can be done on time eventhough the some students having the problems attending the lab. |
| <div align="center">7 | <div align="center">Impact |The first task as been planned out in the timeline of the simulation progress which is the Real time data collection on the physical HAU had been fully confirmed. |
| <div align="center">8| <div align="center">Next Step |The real time measurements on the real physical HAU have to be taken. |

<div align="right">

###### :satellite: ***AEROSPACE DESIGN PROJECT, EAS4947 SEMESTER 1, SESSION 2021/2022*** :satellite:

</div>

<!-- blank line -->
----
<!-- blank line -->
<div align="center">

## **Page 2**

</div>

## Log book 02 [5th November 2021]

|<div align="center"> No. | <div align="center">Content |<div align="center"> Descriptions |
| ------ | ------ | ------ |
|<div align="center"> 1 | <div align="center">Agenda|To form out the Group Progress Gantt Chart. Then, discuss the timeline of the progress in simulations (Real Time Measurements observing, data intake, Catia Programed Drawing or Integrated Designing, Performance Calculations of the HAU, Computational Fluid Dynamics Analysis (CFD), Ansys|
|<div align="center"> 2 | <div align="center">Goals |Looking the design of HAU in real time. The Measurements intake from the modelled exact HAU. Then, selections of the measurements in the Top, side, bottom view.Overall complete design measurements of physical HAU. |
|<div align="center"> 3 | <div align="center">Problem |Figured out that it is complicated to see the design of HAU through virtually, instead have to look for it in real time. The measurements of real time HAU taken are not accurate due to systematic error and random error. |
|<div align="center"> 4 | <div align="center">Decision |Took a visit to Lab H 2.1 to look at the HAU model and the real time design. |
|<div align="center"> 5 | <div align="center">Method |Take the real measurements of the HAU which is available. **Measurements:** Tolerance was set according to the measurements to avoid greater significant on the error involved. |
|<div align="center"> 6|<div align="center"> Justifications |To perform a prototype which looks good in terms of the measurements ratio set up in the CATIA programme. Then, to perform the good performance calculations or reviews on HAU model.To have a good results on the Fluid Dynamics concept. |
| <div align="center">7 |<div align="center"> Impact |Have a good illustrations or imaginations on how to design the HAU in the CATIA (Airframe Design). Besides, allow to think of the possibilities parameters involved (Lift, Drag, Thrust, Weight).Possible CFD outcomes.|
|<div align="center"> 8| <div align="center">Next Step |Designing process will begin in CATIA programme. Finally, have to review on the parameters involved from other subsystem group to have a better idea in performance calculations. |

<div align="right">

###### :satellite: ***AEROSPACE DESIGN PROJECT, EAS4947 SEMESTER 1, SESSION 2021/2022*** :satellite:

</div>

<!-- blank line -->
----
<!-- blank line -->
<div align="center">

## **Page 3**

</div>


## Log book 03 [12th November 2021]

|<div align="center"> No. | <div align="center">Content |<div align="center"> Descriptions |
| ------ | ------ | ------ |
| <div align="center">1 | <div align="center">Agenda|Had designed the HAU airframe in the Computer Aided Designing tools like the CATIA V5. We had discussed the timeline of the simulation's progress in terms of the Real Time Measurements observing, data intake, Catia Programed Drawing or Integrated Designing, Performance Calculations of the HAU, Computational Fluid Dynamics Analysis (CFD), Ansys.|
|<div align="center"> 2 | <div align="center">Goals | To design the airframe design of the HAU Airship with the help of the Computer Aided Tool, CATIA designing. Then, to perform the designing with the ratio selected as to have a good Computational Fluid Dynamics results in future progress. |
| <div align="center">3 | <div align="center">Problem |Figured out that it is complicated to see the design of HAU through virtually, instead have to look for it in real time (Been faced in Week 2).Besides, the real time design the HAU airframe been observed is not enough to simulate the progress. Then, the CATIA could not able to process certain time when designing process took place.  |
|<div align="center"> 4 | <div align="center">Decision |To design the airframe of the airship with a simple notes of measurements.To design the Airframe with the CATIA programme or software. |
| <div align="center">5 | <div align="center">Method |Sketched the design at first to have a good view imagination on the design itself.Began to design the model with the specific ratio of the measurements had been set up. **_Note_**: Tolerance on the measurements was set to avoid greater significant on the error involved.|
| <div align="center">6| <div align="center">Justifications |Will have a good presentation on the whole Airframe design of the HAU itself.To have a good theory or concept on the future design adjustmnent progress through the CATIA V5 programme.To have a good results on the Computational Fluid Dynamics (CFD) and performance results. |
| <div align="center">7 | <div align="center">Impact |The design can be seen clearly and eel easy to noitice the curvature or the outline of the airframe compared to the real life design as it is bigger in size and hard to notice those minor outlines.Able to think and have a imagination as well as the plans to come out certain mmandatory parameters like the Lift, Drag, Thrust and Weight.|
| <div align="center">8| <div align="center">Next Step |The Computational Fluid Dynamics (CFD) process on the performnce of the airship had been planned out. |

<div align="center">

<img src="https://gitlab.com/viknes10/idp-weekly-logbook/-/raw/main/HAU%20Design%20(%20CATIA)" width=350 align=middle>

</div>

<div align="right">

###### :satellite: ***AEROSPACE DESIGN PROJECT, EAS4947 SEMESTER 1, SESSION 2021/2022*** :satellite:

</div>

<!-- blank line -->
----
<!-- blank line -->
<div align="center">

## **Page 4**

</div>
   

## Log book 04 [19th November 2021]

|<div align="center"> No. | <div align="center">Content |<div align="center"> Descriptions |
| ------ | ------ | ------ |
| <div align="center">1 | <div align="center">Agenda|To do the performance theory and analysis on the specimen or model been designed.Computanional Fluid Dyanmics (CFD) analysis on the CATIA designed HAU airframe.|
|<div align="center"> 2 | <div align="center">Goals |To perform and obtain a good and presentable performance analysis. |
|<div align="center"> 3 | <div align="center">Problem |There is lacking of idea on performing the manual overide calculation on performace analysis.There was few parameters could not retrived at certain moment from other subsystem due to other subsystems does not have the detailed results yet|
| <div align="center">4 |<div align="center"> Decision |Had been decided to do the Computational Fluid Dynamics (CFD) by using the ANSYS software. |
|<div align="center"> 5 | <div align="center">Method |Use the design of the HAU Airframe that been designed in the CATIA V5 previously.Transfer the design from the CATIA then render it through ANSYS.**_Note_**: In ANSYS, the design will be choosen to undergo the simulations processes and then the system will provide the pressure contour and all the parameters regards to design.|
| <div align="center">6| <div align="center">Justifications |Will have an idea on what parameters that was actually needed to be focu on instead of just proceed on the manual calculations instead before the right simulations process done.Will have the idea on how the simulation process done.To track any disperencies or error in the reults obtained through ANSYS and perform the adjustment to it. |
| <div align="center">7 | <div align="center">Impact |Have a good perormamce analysis that is presentable.|
| <div align="center">8| <div align="center">Next Step |perform the analysis on the Computanional Fluid Dynamics (CFD) once again to perform the adjustment at the same time. |

|<div align="center"> Documents on Week 4 | <div align="center">File Path |
| ------ | ----------- |
| <div align="center">ANSYS  |<div align="center"> [link](https://gitlab.com/viknes10/idp-weekly-logbook/-/blob/main/ANSYS_SIMULATIONS.pptx) |

<div align="right">

###### :satellite: ***AEROSPACE DESIGN PROJECT, EAS4947 SEMESTER 1, SESSION 2021/2022*** :satellite:

</div>

<!-- blank line -->
----
<!-- blank line -->
<div align="center">

## **Page 5**

</div>


## Log book 05 [26th November 2021]

|<div align="center"> No. |<div align="center"> Content |<div align="center"> Descriptions |
| ------ | ------ | ------ |
| <div align="center">1 |<div align="center"> Agenda|To do the Computational Fluid Dynamics (CFD) testing once again on the design.To do the data collection from the Ansys software for the parameters of the coefficient of lift (C_l), coefficient of drag (c_d) and the moment coefficient (C_m). To collect the weight data of each subsystem and to relate the volume of the airship with all the weight of the subsystem that been provided.|
| <div align="center">2 | <div align="center">Goals |To perform the relation of the volume o the airship and the approximate weight gained from each subsystem. To perform the volume and the weight relation based prooving (calculations).To find the other parameters of forces like the Bouyancy Force, Aerodynamic Lift and the Bouyancy Ratio. |
|<div align="center"> 3 | <div align="center">Problem |Have the problems to for the relation of volume and weight of each susbystem. Faced the problems on the performing the calculations on the voiume and weight relation. Not able to imagine the lift orce form bouyancy concept and the aerodynamic properties form the based on the ANSYS only.|
|<div align="center"> 4 | <div align="center">Decision |To perform the calculations in order to know the exact concept of the volume and weight relations. Find out the Bouyancy Lift, Bouyancy Ratio and the Aerodynamic Lift by reffering to the Fundamentals of the Airship and Design book. |
| <div align="center">5 | <div align="center">Method |Finding the Volume of the airship by using the theory of ellipsoid or the spheroid volume. Find out the total weight or gross weight of each subsystem been tabulated. **_Note_**: The total gross weight for this time was exceeded the 10 kg since it is just approximate. The real gross weight will provided once all the subsystem weight being finalised.|
| <div align="center">6| <div align="center">Justifications |Able to present the relation of the volume and weight of each subsystem together by plotting the graph of Volume vs Weight of susbystem (Kg). Able to know the approximate bouyancy lit, aerodynamic lift and the bouyancy ratio, BR. |
| <div align="center">7 | <div align="center">Impact |The graph of Volume vs Weight of susbystem (Kg) had been genarated together the lift and bouyancy force had been known too.|
|<div align="center"> 8| <div align="center">Next Step |The formuation of relation between the lenght of airship and the lift force. |

| <div align="center">Documents on Week 5 | <div align="center">File Path |
| ------ | ----------- |
|<div align="center"> HAU Performance Calculation Part 1 | <div align="center">[link](https://gitlab.com/viknes10/idp-weekly-logbook/-/blob/main/HAU_Airship_Performance_Part_1__Calc___2_.pdf) |

<div align="center">

<img src="https://gitlab.com/viknes10/idp-weekly-logbook/-/raw/main/Spreadsheets%20Data%20(ANSYS%20and%20Subsystem%20weight)" width=350 align=middle>

</div>

 
<div align="center">

<img src="https://gitlab.com/viknes10/idp-weekly-logbook/-/raw/main/Spreadsheets%20Data%20(Simulation%20Progress%20on%20Graph)" width=350 align=middle>

</div>

<div align="right">

###### :satellite: ***AEROSPACE DESIGN PROJECT, EAS4947 SEMESTER 1, SESSION 2021/2022*** :satellite:

</div>

<!-- blank line -->
----
<!-- blank line -->
<div align="center">

## **Page 6**

</div>

## Log book 06 [3rd December 2021]

| <div align="center">No. |<div align="center"> Content |<div align="center"> Descriptions | 
| ------ | ------ | ------ |
| <div align="center">1 |<div align="center"> Agenda |Had did the Ansys Programming Software Analysis on the steps findimng the CL, CD, Cd0 and Cm. Then, the graph which is volume vs weight and volume vs lift force was plotted.  |
|<div align="center"> 2 | <div align="center">Goals |To find out certain important parameters and do the calibrations to it. Especially, the Ansys results obtained on the  CL, CD, Cd0 and Cm. Then, continuing the calculations part on the finding the lenght of airship and certain parameters like the surface area, finnese ration and the relationship of the lenght with the lift force as well.  |
|<div align="center"> 3 | <div align="center">Problem |The Ansys programe software could not run on laptop due to heavy ram usage to generate the results. The steps on finding the way to calculate the parameters had mentioned above like the lenght of airship, finnese ratio, and the surface area.  |
|<div align="center"> 4 | <div align="center">Decision |The Ansys software analysis had been put under keep in view and without wasting time the progress had been moved on the parameters calculation which is mandatory to some out with the graph plotting. |
| <div align="center">5 | <div align="center">Method |The calculations of the parameters like the Lenght of airship, finnese ratio and the parameters like the surface area had been refered to the points given by Dr. Salah as well as the help from the Fundamental of Airship Design Book. |
| <div align="center">6| <div align="center">Justifications |Able to present the relationship of lenght as weel with the lift force very well withn the graph plotting. |
| <div align="center">7 |<div align="center"> Impact |The progress would be go more details as expected on the certain mandatory parameters that had been missed out on the progress last week or it was unclear and now able to define it.   |
|<div align="center"> 8 | <div align="center">Next Step |The calibration process if taken place based on upcoming progress feedback on the Ansys programme results analysis as well as the calculations made on the parameters finding. |

| <div align="center">Documents on week 6 | <div align="center">File Path |
| ------ | ----------- |
| <div align="center">Simulation Progress Calculation |<div align="center"> [link](https://gitlab.com/viknes10/idp-weekly-logbook/-/blob/main/Simulation_Calculation_Progress_Part_2__1_.pdf) |
| <div align="center">Parameters Table and Formulas | <div align="center">[link](https://docs.google.com/spreadsheets/d/1_hKITENxcHATH8xV-IWAX3jrlcVzm04Xn3GvqWEwM4A/edit?usp=sharing) |

<div align="right">

###### :satellite: ***AEROSPACE DESIGN PROJECT, EAS4947 SEMESTER 1, SESSION 2021/2022*** :satellite:

</div>

<!-- blank line -->
----
<!-- blank line -->
<div align="center">

## **Page 7**

</div>

## Log book 07

| <div align="center">No. | <div align="center">Content |<div align="center"> Descriptions | 
| ------ | ------ | ------ |
| <div align="center">1 |<div align="center"> Agenda |We had planned out to do the calibration on the Ansys results and together with the calculation steps on finding out the major parameters like the lenght, a and the width, b that would be linked to the finding out the weight of the skin relation to the fineness ratio. |
| <div align="center">2 | <div align="center">Goals | The goals are to find out the exact plot of the graph regards to the Lift Coefficient, Cl and Drag Coefficient, Cd vs Angle of Attack, Lift Coefficient, Cl and Drag Coefficient, Cd, Lift Coefficient, Cl and Drag Coefficient, Cd and the calibration on error made to the calculations of the finding out weight of the skin.  |
| <div align="center">3 |<div align="center"> Problem |Problems was found in term of the finding out the similarities or matching the graph plotting that had bee plotted. The Lift Coefficient, Cl and Drag Coefficient, Cd vs Angle of Attack graph does not show the same characteristics had been refered from the Fundamental of Airship and Design Book. Then, the weight per 1 meter square of skin weight found was inccorect due to unknown materials that had been not identified while in the progress together with the confussion occur in the volume statement. |
| <div align="center">4 |<div align="center"> Decision |The Graph of results mainly the Lift Coefficient, Cl and Drag Coefficient, Cd vs Angle of Attack were analysed again. Then, the calculation progress especially on the weight of the skin had to be restructured again from the base.  |
| <div align="center">5 |<div align="center"> Method |The simulation team had taken the decission to step the progress or push the progress into several task by dividing the sub task to every members in the group to find out another data results from Ansys Software by using different Angle of Attack, A.O.A. Then, the calculations part was planned to restructured by looking for the correct weight per 1 meter square of skin weight which mandatory in the simulation calculation. The weight per 1 meter square of skin weight was found in two ways which is by looking into materials catalogue or through Ansys software analysis by finding out the Surface Area of Skin and relation to the total weight of skin. |
| <div align="center">6| <div align="center">Justifications |The presentable results would to get in terms of the Ansys results and together with the graph plotting regards to the Lift Coefficient, Cl and Drag Coefficient, Cd vs Angle of Attack which is mandatory. Then, the approximate weight of the skin can be obtained by finding the exact weight per 1 meter square of skin weight and together with the lenght, a. |
|<div align="center"> 7 | <div align="center">Impact |The main problems faced before in finding out the coefficients can be solved by dividing the task to everyone in doing the Ansys software analysis. The calculations can be calibrated in regards to the weight of skin as well as the measurement lenght, a.    |
|<div align="center"> 8 | <div align="center">Next Step |To plan out for the another calibration on the coefficients result through Ansys software analysisand also the calculation with the help of the lecturer if needed.  |

<div align="right">

###### :satellite: ***AEROSPACE DESIGN PROJECT, EAS4947 SEMESTER 1, SESSION 2021/2022*** :satellite:

</div>

<!-- blank line -->
----
<!-- blank line -->
<div align="center">

## **Page 8**

</div>

## Log book 08

| <div align="center">No. | <div align="center">Content |<div align="center"> Descriptions | 
| ------ | ------ | ------ |
| <div align="center">1 |<div align="center"> Agenda |We had planned out the to do the Ansys programme task as to get the results of the aerodynamics properties based on the various angles been set up. Then, the plotting of the graph for the Lift vs Lenght and the Weight of skin vs Lenght had been constructed out.  |
| <div align="center">2 | <div align="center">Goals |To find out the exact plots of the graph with the correct analysis data.To find out the weight per skin of the airship as it is the missing parameters ad  callibration done from the past results presented to Dr.     |
| <div align="center">3 |<div align="center"> Problem |To find out the exact plots or the trendline of the graph's curve, the some mandatory parameters had been missing like the weight per skin of the airship. The weight per skin of the airship is mandatory that to be included in the calculation dof the weight per skin to find out the surface area.  |
| <div align="center">4 |<div align="center"> Decision |Decided to visit lab and took the reading on the weight per skin with the advice from Dr.  |
| <div align="center">5 |<div align="center"> Method |The weight per skin of the airship had been measured with the folding method of overall skin of the airship and hand it to the weight measuring scale by using the rope.  |
| <div align="center">6| <div align="center">Justifications |The exact weight per skin can be obtained throught a exact measurements intake. The real weight of the skin obtaioned can be included in the calculations and further analysis on the plotting og the graphs which is the Weight of the skin vs Lenght.|
|<div align="center"> 7 | <div align="center">Impact |The graph of the lift vs lenght and the graph on the Weight of skin vs lenght can be plotted at accordingly.   |
|<div align="center"> 8 | <div align="center">Next Step |The correction will have to do unless there is the mistakes in the plots.  |

<div align="right">

###### :satellite: ***AEROSPACE DESIGN PROJECT, EAS4947 SEMESTER 1, SESSION 2021/2022*** :satellite:

</div>

<!-- blank line -->
----
<!-- blank line -->
<div align="center">

## **Page 9**

</div>

## Log book 09

| <div align="center">No. | <div align="center">Content |<div align="center"> Descriptions | 
| ------ | ------ | ------ |
| <div align="center">1 |<div align="center"> Agenda |To do the Ansys configuration for the designed airship with various angles. Besides, to do the graph plotting for the Lift vs Lenght and the Weight of skin vs Lenght graph.|
| <div align="center">2 | <div align="center">Goals |To plot the graph of the relation by showing the curve trendline of the Lift vs Lenght and the Weight of skin vs Lenght graph.   |
| <div align="center">3 |<div align="center"> Problem |The problems was found in the formulating the right equation in the excel to programme it. The graph consist of the some error in the curve fit where the error found out in the plot line it consist of zig-zag line.|
| <div align="center">4 |<div align="center"> Decision |I had decided to take the references from the past student's thesis book and documents. Then, searched up for the right formula to have the final relation where to plot the graph of the Lift and Weight of skin that relate to the lenght.   |
| <div align="center">5 |<div align="center"> Method |I had revisit and redo the calculations made before this with the applying of the right formula that much integrated. |
| <div align="center">6| <div align="center">Justifications |The plot of the Lift and weight per skin vss to the lenght can be plotted. |
|<div align="center"> 7 | <div align="center">Impact |The desired plots can be obtained as it is presentable.    |
|<div align="center"> 8 | <div align="center">Next Step |The correction will have to do unless there is the mistakes in the plots.  |

<div align="right">

###### :satellite: ***AEROSPACE DESIGN PROJECT, EAS4947 SEMESTER 1, SESSION 2021/2022*** :satellite:

</div>

<!-- blank line -->
----
<!-- blank line -->
<div align="center">

## **Page 10**

</div>

## Log book 10

| <div align="center">No. | <div align="center">Content |<div align="center"> Descriptions | 
| ------ | ------ | ------ |
| <div align="center">1 |<div align="center"> Agenda |To finish up the plotting of the graph as been analysed and corrected for few days ago based on the comments given by Dr to improvise it. |
| <div align="center">2 | <div align="center">Goals |To get the right plot of the curve line in the graph of the Lift vs Lenght and the Weight of Skin vs Lenght.   |
| <div align="center">3 |<div align="center"> Problem |A liltle bit of the curve error was found in the curve line of teh graph plotted. |
| <div align="center">4 |<div align="center"> Decision |I had decided to show the plotted graph after many tries was done to remove the error to the Dr Salah.  |
| <div align="center">5 |<div align="center"> Method |I had did the calculations and re-analysed the method again asa well as the included all the equation of weight per skin espcially where those equation wass missing before this. |
| <div align="center">6| <div align="center">Justifications |The right plotting of the graph can be tracked down.|
|<div align="center"> 7 | <div align="center">Impact |The graph of the relation of the Lift and Weight per skin vs the lenght plotted with the right curve line as needed.    |
|<div align="center"> 8 | <div align="center">Next Step |To help the other teams in their work and also the feedback on the simulation task had been done.  |

<div align="right">

###### :satellite: ***AEROSPACE DESIGN PROJECT, EAS4947 SEMESTER 1, SESSION 2021/2022*** :satellite:

</div>

<!-- blank line -->
----
<!-- blank line -->
<div align="center">

## **Page 11**

</div>

## Log book 11

| <div align="center">No. | <div align="center">Content |<div align="center"> Descriptions | 
| ------ | ------ | ------ |
| <div align="center">1 |<div align="center"> Agenda |To show to the Dr. Salah and get the comments on the plots plotted and create the perfect plots for the lift vs lenght graphs for the updated subsystem weight. |
| <div align="center">2 | <div align="center">Goals |To do the perfect fit of the plot that regards to the clean weight of the subsystem exclude the payload weight in the system and then find out the lift of it.    |
| <div align="center">3 |<div align="center"> Problem |The plots plotted arrcorrect but there is some confusion happened where the lift response was inpossible whereby the lenght is much longer than usual found out in the statistical experimental data analysis.   Confusion happened on the lift unit where it iss to be in Kg or Newton terms. |
| <div align="center">4 |<div align="center"> Decision |Revisit the analyis data on the lift and the lenght datas.   |
| <div align="center">5 |<div align="center"> Method |Use the equation derivation in therm to find out the lift is in exactly what unit to be considered. |
| <div align="center">6| <div align="center">Justifications |The right data plots on the Lift vs Lenghtan be obtained and the unit used in the consideration would be fixed. |
|<div align="center"> 7 | <div align="center">Impact |The total grosss weight can be analysed and finally the terms of the gross lift can be analysed through the analysis mathematical model.   |
|<div align="center"> 8 | <div align="center">Next Step |The correction will have to do unless there is the mistakes in the plots.  |

<div align="right">

###### :satellite: ***AEROSPACE DESIGN PROJECT, EAS4947 SEMESTER 1, SESSION 2021/2022*** :satellite:

</div>

<!-- blank line -->
----
<!-- blank line -->
<div align="center">

## **Page 12**

</div>

## Log book 12

| <div align="center">No. | <div align="center">Content |<div align="center"> Descriptions | 
| ------ | ------ | ------ |
| <div align="center">1 |<div align="center"> Agenda |To finalise the plots in the lift and weight per skin section |
| <div align="center">2 | <div align="center">Goals |To come out with the correct data for the lift and the lift-weight per skin. Then, to create the modulation aato finalise the amount of helium needed to filled up.  |
| <div align="center">3 |<div align="center"> Problem |The weight of helium needed was getting bigger when the weight of the subsytem decreased. |
| <div align="center">4 |<div align="center"> Decision |The online tools was used in order to know the weight of helium needed for the balloon to carry the weight. Then, the results was compared with the engineering based calculations on the volume of the airship by using the airship design specifications in excel spreadsheets.   |
| <div align="center">5 |<div align="center"> Method |Used the engineering volume formula of the airship. |
| <div align="center">6| <div align="center">Justifications |To able to get the approximate value of the volume needed in order to have the estimation on the lift section. |
|<div align="center"> 7 | <div align="center">Impact |The right estimation on the lift and the volume of helium can be done.    |
|<div align="center"> 8 | <div align="center">Next Step |To do calibrations on the weight of the helium as well to come out with right data on the airship's lift. |

<div align="right">

###### :satellite: ***AEROSPACE DESIGN PROJECT, EAS4947 SEMESTER 1, SESSION 2021/2022*** :satellite:

</div>

<!-- blank line -->
----
<!-- blank line -->
<div align="center">

## **Page 13**

</div>

## Log book 13

| <div align="center">No. | <div align="center">Content |<div align="center"> Descriptions | 
| ------ | ------ | ------ |
| <div align="center">1 |<div align="center"> Agenda |Finalise the weight of helium needed through google spreadsheets. |
| <div align="center">2 | <div align="center">Goals |To start in the report progress under simulation team and to finalise the results of the data mathematically for the lift data.   |
| <div align="center">3 |<div align="center"> Problem |The susbystem weight had made some changes in which may cause the final helium weight to change. |
| <div align="center">4 |<div align="center"> Decision |Gather the latest subsystem weight in order to update the lift and helium weight data.    |
| <div align="center">5 |<div align="center"> Method |Updated the susbystem weight after the final weighing check throught weighing balance. |
| <div align="center">6| <div align="center">Justifications |To obtain the correct estimation the lift-weight data. |
|<div align="center"> 7 | <div align="center">Impact |New helium weight can be estimated as well the helium needed to be filled.    |
|<div align="center"> 8 | <div align="center">Next Step |Final cross check for the airship flying week. |


<div align="right">

###### :satellite: ***AEROSPACE DESIGN PROJECT, EAS4947 SEMESTER 1, SESSION 2021/2022*** :satellite:

</div>

<!-- blank line -->
----
<!-- blank line -->
<div align="center">

## **Page 14**

</div>

## Log book 14

| <div align="center">No. | <div align="center">Content |<div align="center"> Descriptions | 
| ------ | ------ | ------ |
| <div align="center">1 |<div align="center"> Agenda |To state all the finalised mathematical data to Dr, Salah in order to check the mistakes as weel the final updates.Then, to check the weighing of the all susbystems attached to the airship. |
| <div align="center">2 | <div align="center">Goals |To finalise the helium weight after susbsytem weight check.   |
| <div align="center">3 |<div align="center"> Problem |have theproblem in the susbystem weight check. |
| <div align="center">4 |<div align="center"> Decision |Attached all the susbystem weight not included the payload onto the airship. Then, the airship was tied and left to hang with the weight balance.   |
| <div align="center">5 |<div align="center"> Method |The formula and the centre gravity method was determinded in order to know the exact gravity point on the airship. |
| <div align="center">6| <div align="center">Justifications |To get the correct estimation of full subsystem weight and the theoritical weight nalaysis can be compared.  |
|<div align="center"> 7 | <div align="center">Impact |The right helium weight v=can be determined in order to fill up in the flying day.   |
|<div align="center"> 8 | <div align="center">Next Step |Fly the airship on the day itself with the cross checks on the lift data as well the helium weight.  |

###### :satellite: ***AEROSPACE DESIGN PROJECT, EAS4947 SEMESTER 1, SESSION 2021/2022*** :satellite:

</div>

<!-- blank line -->
----
<!-- blank line -->
<div align="center">

## **Page 14**

</div>
